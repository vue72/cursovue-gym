const app = new Vue({
    el: '#app',
    data: {
        titulo: "GYM con Vue",
        tareas: [],
        nuevaTarea: ''
    },
    methods: {
        agregarTarea: function () {
            this.tareas.push({
                nombre: this.nuevaTarea,
                estado: false
            });
            this.nuevaTarea = '';
            this.storage();
        },
        editarTarea: function (index) {
            // this.tareas[index].estado = !this.tareas[index].estado;
            this.tareas[index].estado = true;
            this.storage();
        },
        eliminarTarea: function (index) {
            this.tareas.splice(index, 1);
            this.storage();
        },
        storage: function () {
            localStorage.setItem('gym-vue', JSON.stringify(this.tareas));
        }
    },
    created: function () {
        let datosDB = JSON.parse(localStorage.getItem('gym-vue'));
        this.tareas = (datosDB === null) ? [] : datosDB;
    }
})